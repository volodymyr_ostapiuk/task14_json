package com.ostapiuk;

import com.ostapiuk.model.json.Parser;

public class Main {
    public static void main(String[] args) {
        Parser parser = new Parser();
        parser.startParcing();
    }
}
