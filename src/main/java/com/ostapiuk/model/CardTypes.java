package com.ostapiuk.model;

public class CardTypes {
    private String type;
    private boolean isSent;

    public CardTypes() {
    }

    public CardTypes(String newType, boolean sent) {
        type = newType;
        isSent = sent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setSent(boolean sent) {
        isSent = sent;
    }

    @Override
    public String toString() {
        return "CardTypes{" +
                "type='" + type + '\'' +
                ", isSent=" + isSent +
                '}';
    }
}
