package com.ostapiuk.model.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ostapiuk.model.Card;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class JSONJacksonParser {
    private ObjectMapper objectMapper;

    public JSONJacksonParser() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Card> getCardList(File jsonFile) {
        Card[] cards = new Card[0];
        try {
            cards = objectMapper.readValue(jsonFile, Card[].class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Arrays.asList(cards);
    }
}
