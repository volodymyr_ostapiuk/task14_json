package com.ostapiuk.model.json;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.ostapiuk.model.Card;
import com.ostapiuk.model.comparator.CardComparator;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class Parser {
    public void startParcing() {
        File jsonFile = new File("src/main/resources/json/card.json");
        File jsonSchemaFile = new File("src/main/resources/json/cardSchema.json");
        JSONValidator.validateSchema(jsonSchemaFile, jsonFile);
        try {
            JSONNewValidator.validate(jsonFile, jsonSchemaFile);
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
        JSONJacksonParser parser = new JSONJacksonParser();
        printList(parser.getCardList(jsonFile));
    }

    private void printList(List<Card> cards) {
        Collections.sort(cards, new CardComparator());
        System.out.println("Jackson: sorted by year");
        for (Card card : cards) {
            System.out.println(card);
        }
    }
}
